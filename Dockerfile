# DOCKER-VERSION 1.10.3
FROM centos:6.6

# Folder Paths
# sources 	/usr/src
# certs		/etc/nginx/ssl
# logs  	/var/log/nginx
# pid   	/var/run
# work 		/etc/nginx

#Update yum and install basic tools
RUN yum -y update && yum clean all
RUN yum -y install wget curl vim gcc gcc-c++ make tar

#Install pcre from source
RUN cd /usr && mkdir -p src && cd src && \
	wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.38.tar.gz && \
	tar -zxf pcre-8.38.tar.gz && \
	cd pcre-8.38 && \
	 ./configure  && \
	 make && make install

#Install xlib from source
RUN cd /usr/src && \
	wget http://zlib.net/zlib-1.2.8.tar.gz && \
	tar -zxf zlib-1.2.8.tar.gz && \
	cd zlib-1.2.8 && \
	./configure && \
	make && make install

#Install openssl from source (note that openssl-1.0.2f had conflict issues)
RUN cd /usr/src && \
	wget http://www.openssl.org/source/openssl-1.0.1g.tar.gz && \
	tar -zxf openssl-1.0.1g.tar.gz && \
	cd openssl-1.0.1g && \
	./config --prefix=/usr/local --openssldir=/usr/local/openssl && \
	make && make install

#Install nginx
RUN cd /usr/src && \
	wget http://nginx.org/download/nginx-1.10.0.tar.gz && \
	tar zxf nginx-1.10.0.tar.gz && \
	cd nginx-1.10.0 && \
	./configure \
	--user=nginx                          \
	--group=nginx                         \
	--prefix=/etc/nginx                   \
	--sbin-path=/usr/sbin/nginx           \
	--conf-path=/etc/nginx/nginx.conf     \
	--pid-path=/var/run/nginx.pid         \
	--lock-path=/var/run/nginx.lock       \
	--error-log-path=/var/log/nginx/error.log \
	--http-log-path=/var/log/nginx/access.log \
	--with-openssl=../openssl-1.0.1g      \
	--with-pcre=../pcre-8.38              \
	--with-zlib=../zlib-1.2.8             \
	--with-http_gzip_static_module        \
	--with-http_stub_status_module        \
	--with-http_ssl_module                \
	--with-pcre                           \
	--with-file-aio                       \
	--with-http_realip_module             \
	--without-http_scgi_module            \
	--without-http_uwsgi_module           \
	--without-http_fastcgi_module         \
	--with-http_auth_request_module    && \
	make && make install


# Define nginx user
RUN useradd --shell /sbin/nologin nginx

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

# Define working directory.
WORKDIR /etc/nginx

# Backup existing config
RUN mv nginx.conf nginx.conf.old

# Copy custom configuration file from the current directory
COPY nginx.conf nginx.conf

# Copy custom certifcates files from the SSL directory
COPY ssl ssl/

EXPOSE 80 8080 443
CMD ["nginx", "-g", "daemon off;"]